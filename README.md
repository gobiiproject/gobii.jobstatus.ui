# Gobii jobstatus UI

## Get deployed fast

Get the latest development version running with minimum fuss:
`docker run -d -p 4200:4200  -e PORT=4200 -e HOST=localhost -e BASEURL=localhost:8083 plantandfood/gobii.jobstatus.ui:qa`

## Development 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli).

#### Local Server

Run `ng serve` or `yarn start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

#### Docker Build

Before pushing code make sure the Docker version works not just the local server

`docker build .`  
`docker run -d -p 4200:4200   -e PORT=4200 -e HOST=localhost -e BASEURL=localhost:8083 plantandfood/gobii.jobstatus.ui:qa`

#### Code scaffolding
Generate a new module

`ng generate module modules/module-name --routing` (--routing for creating routerModule)

Generate a new component and required files

`ng generate component modules/component-name`

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

#### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

#### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

#### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

#### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

