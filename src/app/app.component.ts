import { Component, OnInit } from '@angular/core';
import { NavLinks } from './models/nav-link.model';
import { appNavLinks } from './app-nav-links';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'Gobii jobstatus';
  user: any = { name: 'Demo', user: 'demo_user' };
  links: NavLinks = appNavLinks;

  constructor(
    ) {}

  ngOnInit() {
  }
}
