import { NavLinks } from './models/nav-link.model';

export const appNavLinks: NavLinks = [
  { path: '/dashboard', label: 'Dashboard', ligature: 'dashboard' }
];
