import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgxLibCommonModule } from 'ngx-lib-common';
import { NgxLibMaterialModule } from 'ngx-lib-material';
import { NgxLibStylesModule } from 'ngx-lib-styles';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    // Common libraries
    NgxLibCommonModule,
    NgxLibStylesModule,
    NgxLibMaterialModule
  ],
  exports: [
    CommonModule,
    BrowserModule,
    RouterModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    NgxLibCommonModule,
    NgxLibStylesModule,
    NgxLibMaterialModule,
  ],
  declarations: [
  ],
  providers: []
})
export class SharedModule { }
